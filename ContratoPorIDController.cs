﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ContratoPorIDController : ControllerBase
    {
        private readonly SAESSContext _context;

        public ContratoPorIDController(SAESSContext context)
        {
            _context = context;
        }

        [HttpGet("{Id}")]
        public IEnumerable<ContratoDto> GetContratoPorID(int Id)
        {
            #region Servicios            
            //Consultar las Servicios
            List<ServicioDto> LServicio = new List<ServicioDto>();
            var Servicio = from c in _context.Contrato
                           join ss in _context.SolicitudServicio on
                           c.ContId equals ss.ContId
                           join sc in _context.Solicitudconservicio on
                           ss.SoseId equals sc.SoseId
                           join s in _context.Servicio on
                           sc.SerId equals s.SerId

                           where c.ContId == Id
                            && c.ContActivo == true
                            && ss.SoseActivo == true
                            && sc.SocseActivo == true
                            && s.SerActivo == true
                           select s;

            foreach (var ob in Servicio)
            {
                LServicio.Add(new ServicioDto
                {
                    SerId = ob.SerId,
                    SerNombre = ob.SerNombre,
                    SerDescripcion = ob.SerDescripcion,
                    SerCodigo = ob.SerCodigo
                });
            }
            #endregion

            #region solicitud Servicios            
            //Consultar las Servicios
            List<SolicitudServicioDto> LSServicio = new List<SolicitudServicioDto>();
            var SServicio = from c in _context.Contrato
                            join ss in _context.SolicitudServicio on
                            c.ContId equals ss.ContId

                            where c.ContId == Id
                            && c.ContActivo == true
                            && ss.SoseActivo == true
                            select ss;

            foreach (var ob in SServicio)
            {
                LSServicio.Add(new SolicitudServicioDto
                {
                    SoseId = ob.SoseId,                   
                    SoseNumero = ob.SoseNumero,
                    Precio = ob.SosePrecio
                });
            }
            #endregion

            #region zona            
            //Consultar las Servicios
            List<ZonaDto> LZona = new List<ZonaDto>();
            var zona = from c in _context.Contrato
                       join p in _context.Persona on
                       c.PersId equals p.PersId
                       join pcz in _context.Personaconzona on
                       p.PersId equals pcz.PersId
                       join z in _context.Zona on
                       pcz.ZonaId equals z.ZonaId

                       where c.ContId == Id
                        && c.ContActivo == true
                        && pcz.PezoActivo == true
                        && z.ZonaActivo == true
                       select z;

            foreach (var ob in zona)
            {
                LZona.Add(new ZonaDto
                {
                    ZonaId = ob.ZonaId,
                    ZonaNombre = ob.ZonaNombre
                });
            }
            #endregion zona

            #region persona            
            //Consultar las Servicios
            List<PersonaDto> Lpersona = new List<PersonaDto>();
            var Persona = from c in _context.Contrato
                          join p in _context.Persona on
                          c.PersId equals p.PersId

                          where c.ContId == Id
                           && c.ContActivo == true
                          select p;

            foreach (var ob in Persona)
            {
                Lpersona.Add(new PersonaDto
                {
                    PersNumDocumento = ob.PersNumDocumento,
                    PersNombre = ob.PersNombre,
                    PersApellido = ob.PersApellido,
                    PersEmail = ob.PersEmail,
                    PersCelular = ob.PersCelular,
                    PersDireccion = ob.PersDireccion,
                    PersTelefono = ob.PersTelefono
                });
            }
            #endregion persona

            var modelo = from c in _context.Contrato
                         join cc in _context.ClasificacionCliente on
                         c.ClclId equals cc.ClclId
                         join ec in _context.EstadoContrato on
                         c.EscoId equals ec.EscoId
                         join cm in _context.CableModen on
                         c.ContId equals cm.ContId

                         where c.ContId == Id

                         select new ContratoDto
                         {
                             ContId = c.ContId,
                             ClclId = c.ClclId,
                             SectId = c.SectId,
                             EscoId = c.EscoId,
                             ContNumero = c.ContNumero,
                             ContCuota = c.ContCuota,
                             ContDireccion = c.ContDireccion,
                             ContTelefono = c.ContTelefono,
                             ContFechaInstalacion = c.ContFechaInstalacion,                            
                             PersId = c.PersId,
                             CabModenId = cm.CabModenId,
                             CabMarca = cm.CabMarca,
                             CabIdMac = cm.CabIdMac,
                             CabSerial = cm.CabSerial,
                             ClclNombre = cc.ClclNombre,
                             ClclDescripcion = cc.ClclDescripcion,
                             ClclExento = cc.ClclExento,
                             EscoNombre = ec.EscoNombre,
                             EscoDescripcion = ec.EscoDescripcion,
                             ContFechaCreacion = c.ContFechaCreacion,
                             Responsable = c.ContResponsable,
                             Servicio = LServicio,
                             Persona = Lpersona,
                             Zona = LZona,
                             SolicitudServicio = LSServicio
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Contrato, ContratoDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<ContratoDto>>(modelo);
        } 
    }
}
