﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Dto;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ContratoPorDocumentoPersonaController : ControllerBase
    {
        private readonly SAESSContext _context;

        public ContratoPorDocumentoPersonaController(SAESSContext context)
        {
            _context = context;
        }


        [HttpGet("{Numero}")]
        public IEnumerable<ContratoDto> GetContratoPorDocumentoPersona(string Numero)
        {
            #region solicitud Servicios            
            //Consultar las Servicios
            List<SolicitudServicioDto> LSolicitudServicio = new List<SolicitudServicioDto>();

            var SolicitudServicio = from p in _context.Persona
                           join c in _context.Contrato on
                           p.PersId equals c.PersId
                           join ss in _context.SolicitudServicio on
                           c.ContId equals ss.ContId
                           
                           where p.PersNumDocumento == Numero
                           && p.PersActivo == true
                           && c.ContActivo == true
                           && ss.SoseActivo == true

                           select ss;

            foreach (var ob in SolicitudServicio)
            {
                LSolicitudServicio.Add(new SolicitudServicioDto
                {
                    SoseId = ob.SoseId,   
                    FactId = ob.FactId,
                    SoseEmpleado = ob.SoseEmpleado,
                    SoseNumero = ob.SoseNumero,
                    SoseDescripcion = ob.SoseDescripcion,
                    SoseFechaEjecucion = ob.SoseFechaEjecucion,
                    SosePrecio = ob.SosePrecio
                });
            }
            #endregion

            #region zona            
            //Consultar las Servicios
            List<ZonaDto> LZona = new List<ZonaDto>();
            var zona = from c in _context.Contrato
                       join p in _context.Persona on
                       c.PersId equals p.PersId
                       join pcz in _context.Personaconzona on
                       p.PersId equals pcz.PersId
                       join z in _context.Zona on
                       pcz.ZonaId equals z.ZonaId

                       where p.PersNumDocumento == Numero
                        && c.ContActivo == true
                        && pcz.PezoActivo == true
                        && z.ZonaActivo == true
                       select z;

            foreach (var ob in zona)
            {
                LZona.Add(new ZonaDto
                {
                    ZonaId = ob.ZonaId,
                    ZonaNombre = ob.ZonaNombre
                });
            }
            #endregion zona

            #region persona            
            //Consultar las Servicios
            List<PersonaDto> Lpersona = new List<PersonaDto>();
            var Persona = from c in _context.Contrato
                          join p in _context.Persona on
                          c.PersId equals p.PersId

                          where p.PersNumDocumento == Numero
                           && c.ContActivo == true
                          select p;

            foreach (var ob in Persona)
            {
                Lpersona.Add(new PersonaDto
                {
                    PersNumDocumento = ob.PersNumDocumento,
                    PersNombre = ob.PersNombre,
                    PersApellido = ob.PersApellido,
                    PersEmail = ob.PersEmail,
                    PersCelular = ob.PersCelular,
                    PersDireccion = ob.PersDireccion,
                    PersTelefono = ob.PersTelefono
                });
            }
            #endregion persona

            #region Servicios            
            //Consultar las Servicios
            List<ServicioDto> LServicio = new List<ServicioDto>();
            var Servicio = from c in _context.Contrato
                           join p in _context.Persona on
                           c.PersId equals p.PersId
                           join ss in _context.SolicitudServicio on
                           c.ContId equals ss.ContId
                           join sc in _context.Solicitudconservicio on
                           ss.SoseId equals sc.SoseId
                           join s in _context.Servicio on
                           sc.SerId equals s.SerId

                           where p.PersNumDocumento == Numero
                            && c.ContActivo == true
                            && ss.SoseActivo == true
                            && s.SerActivo == true
                            && s.SerActivo == true
                           select s;

            foreach (var ob in Servicio)
            {
                LServicio.Add(new ServicioDto
                {
                    SerId = ob.SerId,
                    SerNombre = ob.SerNombre,
                    SerDescripcion = ob.SerDescripcion,
                    SerCodigo = ob.SerCodigo,
                    SerValor = ob.SerValor,
                    EmprId = ob.EmprId
                });
            }
            #endregion

            var modelo = from p in _context.Persona
                         join c in _context.Contrato on
                         p.PersId equals c.PersId
                         join cc in _context.ClasificacionCliente on
                         c.ClclId equals cc.ClclId
                         join ec in _context.EstadoContrato on
                         c.EscoId equals ec.EscoId
                         join cm in _context.CableModen on
                         c.ContId equals cm.ContId

                         where p.PersNumDocumento == Numero
                         && p.PersActivo == true
                         && c.ContActivo == true
                         && cc.ClclActivo == true
                         && ec.EscoActivo == true
                         && cm.CabActivo == true

                         select new ContratoDto
                         {
                             ContId = c.ContId,
                             ClclId = c.ClclId,
                             SectId = c.SectId,
                             EscoId = c.EscoId,
                             ContNumero = c.ContNumero,
                             ContCuota = c.ContCuota,
                             ContDireccion = c.ContDireccion,
                             ContTelefono = c.ContTelefono,
                             ContFechaInstalacion = c.ContFechaInstalacion,                             
                             PersId = c.PersId,
                             CabModenId = cm.CabModenId,
                             CabMarca = cm.CabMarca,
                             CabIdMac = cm.CabIdMac,
                             CabSerial = cm.CabSerial,
                             ClclNombre = cc.ClclNombre,
                             ClclDescripcion = cc.ClclDescripcion,
                             ClclExento = cc.ClclExento,
                             EscoNombre = ec.EscoNombre,
                             EscoDescripcion = ec.EscoDescripcion,
                             ContFechaCreacion = c.ContFechaCreacion,
                             Responsable = c.ContResponsable,
                             Servicio = LServicio,
                             Persona = Lpersona,
                             Zona = LZona,
                             SolicitudServicio = LSolicitudServicio
                         };

            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Contrato, ContratoDto>(); });
            IMapper mapper = config.CreateMapper();
            return mapper.Map<IEnumerable<ContratoDto>>(modelo);
        }
    }
}
